<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<br> <br>
			<div class="row center">
				<div class="col s4">
					<c:if test="${searchWord != null}">
						<a
							href="ItemSearchResult?search_word=${searchWord}&page_num=${pageNum}"
							class="btn waves-effect waves-light">Search Results </a>
					</c:if>
				</div>
				<div class="col s4">
					<h5 class=" col s12 light">Product Details</h5>
				</div>
				<div class="col s4">
					<form action="ItemAdd" method="POST">
						<input type="hidden" name="item_id" value="${item.id}">
						<button class="btn waves-effect waves-light" type="submit"
							name="action">
							Add to Cart <i class="material-icons right">add_shopping_cart</i>
						</button>
					</form>
				</div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col s6">
					<div class="card">
						<div class="card-image">
							<img id="preview" src="img/${item.fileName}">
						</div>
					</div>
				</div>
				<div class="col s6">
					<c:if test="${userId != 1 }">
						<h4>${item.name}</h4>
						<h5>${item.formatPrice}円</h5>
						<p>${item.detail}</p>
					</c:if>
					<c:if test="${userId == 1 }">
						<form method="post" action="Item" enctype="multipart/form-data"
							class="form-horizontal">
							<!-- ここがイメージのインプット-->
							<input type="file" id="img" accept="image/*" name="file" />
							<script type="text/javascript">
								document
										.getElementById('img')
										.addEventListener(
												'change',
												function(e) {
													var file = e.target.files[0];
													var blobUrl = window.URL
															.createObjectURL(file);
													var img = document
															.getElementById('preview');
													img.src = blobUrl;
												});
							</script>

							<p>
								<input type="hidden" value="${item.id}" name="id" id="id">


							</p>

							<h4>
								<input value="${item.name}" name="name" id="name">
							</h4>
							<h5>
								<input value="${item.formatPrice}" name="price" id="price">
							</h5>
							<p>
								<input value="${item.detail}" name="detail" id="detail">
							</p>
							<button class="btn  waves-effect waves-light  col s6 offset-s3"
								type="submit" name="confirmButton" value="update">Confirm</button>
						</form>
					</c:if>
				</div>
			</div>
		</div>
		<jsp:include page="/baselayout/footer.jsp" />
	</div>
</body>
</html>