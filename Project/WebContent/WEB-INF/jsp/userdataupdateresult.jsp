<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新完了</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">Profile Update Successful</h5>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<div class="row">
							<div class="input-field col s6">
								<input type="text" value="${udb.name}" readonly> <label>Name</label>
							</div>
							<div class="input-field col s6">
								<input type="text" value="${udb.loginId}" readonly> <label>Login ID</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input type="text" value="${udb.address}" readonly> <label>Address</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">Updated</p>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<a href="UserData" class="btn waves-effect waves-light  col s4 offset-s4">Back to Profile</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>