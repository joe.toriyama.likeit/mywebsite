<%@	page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="date" class="java.util.Date"/>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入履歴詳細</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h4 class=" col s12 light">Purchase Details</h4>
		</div>
		<!--  購入 -->
		<div class="row">
			<div class="col s10 offset-s1">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table>
							<thead>
								<tr>
									<th class="center" style="width: 20%;">Purchase Date</th>
									<th class="center">Delivery Method</th>
									<th class="center" style="width: 20%">Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="center"><fmt:formatDate value="${buy.buyDate}" pattern="yyyy年MM月dd日 kk時mm分ss秒" /><br></td>
									<%-- <fmt:formatDate value="${buy.buyDate}" pattern="yyyy年MM月dd日（E） a KK時mm分ss秒" /><br>  --%>

									<td class="center">${buy.deliveryMethodName}</td>
									<td class="center">${buy.totalPrice}円</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- 詳細 -->
		<div class="row">
			<div class="col s10 offset-s1">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table class="bordered">
							<thead>
								<tr>
									<th class="center">Product Name</th>
									<th class="center" style="width: 20%">Price</th>
								</tr>
							</thead>
							<tbody>
							<c:forEach var="product" items="${buyDataBeansNameList}">
								<tr>
									<td class="center">${product.name}</td>
									<td class="center">${product.price}円</td>
								</tr>
								</c:forEach>

								<tr>
								<td class="center">${buy.deliveryMethodName}</td>
								<td class="center">${buy.deliveryMethodPrice}円</td>
								</tr>

							</tbody>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>